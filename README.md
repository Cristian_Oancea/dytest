## Requirements
In order to run the code that tests the DY you need to setup a selenium grid with two OS:
-WINDOWS with chrome, firefox, microsoft edge and internet explorer -MAC with safari, chrome, firefox
The two computers must be connected to the same network

## Creating the selenium grid:

MAC:
	- download and install Chrome, Firefox and update Safari if needed
	- download latest SeleniumGridExtras from https://github.com/groupon/Selenium-Grid-Extras/releases
	- go to the folder where you downloaded, open a terminal and write java -jar SeleniumGridExtras-version.jar ( replace -version with whatever the name of the file is ) and follow the steps needed to start a hub and a node ( option 3 ), with safari, chrome and firefox

WINDOWS:
	- sadly, SeleniumGridExtras does not install at the moment Microsoft Edge so you need to setup windows node directly with selenium standalone server. If you don't want Edge, you can setup a node only with SeleniumGridExtras and connect it to the MAC but you will only have chrome, firefox and internet explorer.
	Download latest version of selenium standalone server from https://www.seleniumhq.org/download/
	- download and install Edge, Chrome. Firefox and update IE if needed
	- download th latest browsers corresponding drivers as follow:
		- chrome: http://chromedriver.chromium.org/
		- firefox: https://github.com/mozilla/geckodriver/releases
		- edge: https://docs.seleniumhq.org/download/
		- ie: https://github.com/SeleniumHQ/selenium/wiki/InternetExplorerDriver
	- get all drivers and selenium standalone server in the same folder
	- open a terminal, go to the folder where you have drivers and selenium server and write the following command ( note that you need to change the path of drivers with your actual path and hub address with the MAC's address along with the name of the selenium server you downloaded since versions may differ):

	java -Dwebdriver.ie.driver="C:\Users\Windeln\Downloads\IEDriverServer.exe" -Dwebdriver.edge.driver="C:\Users\Windeln\Downloads\MicrosoftWebDriver.exe" -Dwebdriver.gecko.driver="C:\Users\Windeln\Downloads\geckodriver.exe" -Dwebdriver.chrome.driver="C:\Users\Windeln\Downloads\chromedriver.exe" -jar selenium-server-standalone-3.141.59.jar -port 5556 -role node -hub http://10.10.102.33:4444/grid/register -browser "browserName=internet explorer, platform=WINDOWS, maxInstances=3" -browser "browserName=MicrosoftEdge, platform=WINDOWS, maxInstances=3" -browser "browserName=firefox, platform=WINDOWS, maxInstances=3" -browser "browserName=chrome, platform=WINDOWS, maxInstances=3"

	This should create a node with 3 instances of each browser and connect to the hub on mac.

You can check that the nodes have been created correctly by navigating to http://localhost:4444/grid/console# from the MAC (hub computer) or replace 'localhost' with the local Ip of the hub computer and navigate to that URL form any computer connected to the network.

## Using the code

	- you also need to make changes in the code, at Functions.py change the global variable ip to whatever the hub's LOCAL IP is ( if your MAC IP is 10.10.100.25 change ip = "10.10.100.25:4444/wd/hub"), you can get your MAC IP by writting ifconfig in terminal or navigating to https://www.whatismyip.com/
	- you need to change the links that you want the code to test so in SinglePageTest.py you need to change the global variable url to whatever links you want to test and in Functions.py change variable testLink to the shop where you want to test the PDPs ( if you want to test on DE change testLink = 'https://www.windeln.de/search/?q=*' )