import time
from selenium.common.exceptions import ElementNotVisibleException
from selenium import webdriver
import unittest
from concurrencytest import ConcurrentTestSuite, fork_for_tests



testLink = 'https://www.bebitus.fr/search/?q=*'
rightArrowXpath = '//div[contains(@id, "dy-module")]//div[2]/div[3]'
leftArrowXpath = '//div[contains(@id, "dy-module")]//div[2]/div[1]'

failMsg = ['chrome/MAC', 'firefox/MAC', 'safari/MAC', 'edge/WIN', 'chrome/WIN', 'firefox/WIN', 'IE/WIN']
driver = []
ip = "10.10.102.33:4444/wd/hub"


def drivers():
    try:

        driver.append(
            webdriver.Remote(command_executor=ip,
                             desired_capabilities={"browserName": "chrome", "platform": "MAC"}))

        driver.append(webdriver.Remote(command_executor=ip,
                                       desired_capabilities={"browserName": "firefox", "platform": "MAC"}))

        driver.append(webdriver.Remote(command_executor=ip,
                                       desired_capabilities={"browserName": "safari", "platform": "MAC"}))

        driver.append(webdriver.Remote(command_executor=ip,
                                       desired_capabilities={"browserName": "MicrosoftEdge"}))

        driver.append(webdriver.Remote(command_executor=ip,
                                       desired_capabilities={'browserName': 'chrome', 'platform': 'WINDOWS',
                                                             'platformName': 'WINDOWS'}))

        driver.append(webdriver.Remote(command_executor=ip,
                                       desired_capabilities={'browserName': 'firefox', 'platform': 'WINDOWS',
                                                              'platformName': 'WINDOWS'}))


        print('setup')
    except:
        print('error', '**********setUp failed**********')


def destroy():
    try:
        # closing chromedriver, this runs after every test
        for i in range(6):
            driver[i].quit()
    except:
        print('**************Failed to do tearDown***************')


def clickPrivacyButton(browser):
    try:
        browser.find_element_by_xpath('//button[contains(@id, "footer_tc_privacy_button")]').click()
        time.sleep(2)
    except:
        print('failed to close privacy banner')


def initialSetup(browser):
    browser.implicitly_wait(30)
    browser.get(testLink)
    time.sleep(10)
    clickPrivacyButton(browser)


def getLinksFromSearchResult(browser):
    ele = []
    url = []
    for i in range(120):
        try:
            ele.append(browser.find_element_by_xpath(
                '//div[contains(@id, "productListing")]/article[' + str(i + 1) + ']/section/div[1]/div[1]'))
            url.append(str(ele[i].get_attribute('data-product-link')) + '?test_tania')
        except:
            print('failed ele is: ' + str(ele[i].get_attribute('data-product-link')) + ' and i is: ' + str(i))
    return url


def addToCart(browser):
    try:
        browser.find_element_by_id('btn_add_to_cart').click()
        time.sleep(5)
    except ElementNotVisibleException:
        print('cannot add to cart, probably the product is out of stock')
    except:
        print('failed to add to cart')


def clickArrows(browser):
    try:
        browser.find_element_by_xpath(rightArrowXpath).click()
        time.sleep(2)
        browser.find_element_by_xpath(leftArrowXpath).click()
        time.sleep(2)
    except:
        pass


def goToPage(browser, url):
    time.sleep(1)
    browser.get(url)
    time.sleep(5)


def checkDY(browser, testNR, url):
    try:
        browser.find_element_by_xpath('//div[contains(@id, "dy-module")]')
        clickArrows(browser)
    except:
        print('failed to find DY or failed to click arrows on ' + failMsg[testNR] + ' at page ' + str(url))