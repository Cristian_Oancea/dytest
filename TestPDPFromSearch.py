from Functions import *




class DY(unittest.TestCase):

    def runTests(self, testNR):
        initialSetup(driver[testNR])
        url = getLinksFromSearchResult(driver[testNR])
        for i in range(len(url)):
            goToPage(driver[testNR], url[i])
            addToCart(driver[testNR])
            checkDY(driver[testNR], testNR, url[i])

    def test_chromeMAC(self):
        self.runTests(0)

    def test_firefoxMAC(self):
        self.runTests(1)

    def test_safariMAC(self):
        self.runTests(2)


    def test_edgeWIN(self):
        self.runTests(3)

    def test_chromeWIN(self):
        self.runTests(4)

    def test_firefoxWIN(self):
        self.runTests(5)

    def test_IEWIN(self):
        self.runTests(6)



if __name__ == "__main__":
    drivers()
    suite = unittest.TestLoader().loadTestsFromTestCase(DY)
    runner = unittest.TextTestRunner()
    # runner.run(suite)     # uncomment this to run one test at a time and comment the two lines below
    concurrent_suite = ConcurrentTestSuite(suite, fork_for_tests(7))
    runner.run(concurrent_suite)
    destroy()
