from Functions import *


url = ['https://www.bebitus.com/panales-cambiador/bolsos-cambiadores/?test_katharina',

       'https://www.windeln.ch/erotik/?test_katharina',

       'https://www.windeln.ch/baby-nahrung/milchnahrung/?test_katharina',

       'https://www.bebitus.pt/fraldas-trocador/?test_tania',
       'https://www.bebitus.fr/couches-changes/lingettes/?test_tania1',
       'https://www.bebitus.pt/fraldas-trocador/?test_tania',
       'https://www.bebitus.fr/alimentation/?test_tania'
       ]

class DY(unittest.TestCase):

    def checkMultiplePages(self, testNR):
        initialSetup(driver[testNR])
        for i in range(len(url)):
            goToPage(driver[testNR], url[i])
            checkDY(driver[testNR], testNR, url[i])

    def test_chromeMAC(self):
        self.checkMultiplePages(0)

    def test_firefoxMAC(self):
        self.checkMultiplePages(1)

    def test_safariMAC(self):
        self.checkMultiplePages(2)

    def test_edgeWIN(self):
        self.checkMultiplePages(3)

    def test_chromeWIN(self):
        self.checkMultiplePages(4)

    def test_firefoxWIN(self):
        self.checkMultiplePages(5)




if __name__ == "__main__":
    drivers()
    suite = unittest.TestLoader().loadTestsFromTestCase(DY)
    runner = unittest.TextTestRunner()
    # runner.run(suite)     # uncomment this to run one test at a time and comment the two lines below
    concurrent_suite = ConcurrentTestSuite(suite, fork_for_tests(7))
    runner.run(concurrent_suite)
    destroy()
