from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import unittest

from concurrencytest import ConcurrentTestSuite, fork_for_tests
import time


driver = []
ip = "10.10.102.33:4444/wd/hub"
testLink = 'https://www.bebitus.fr/?test_tania'
#arrowXpath = '//div[contains(@class, "arrow-next")]'               #on PDP
arrowXpath = '//*[@id="dy-module-365575"]/div[2]/div[3]'           #on mainpage
#arrowXpath = '//div[contains(@class, "arrow-next")]'


def drivers():
    try:

        driver.append(
            webdriver.Remote(command_executor=ip,
                             desired_capabilities={"browserName": "chrome", "platform": "MAC"}))

        driver.append(webdriver.Remote(command_executor=ip,
                                       desired_capabilities={"browserName": "firefox", "platform": "MAC"}))

        driver.append(webdriver.Remote(command_executor=ip,
                                       desired_capabilities={"browserName": "safari", "platform": "MAC"}))

        driver.append(webdriver.Remote(command_executor=ip,
                                       desired_capabilities={"browserName": "MicrosoftEdge"}))

        driver.append(webdriver.Remote(command_executor=ip,
                                       desired_capabilities={'browserName': 'chrome', 'platform': 'WINDOWS',
                                                             'platformName': 'WINDOWS'}))

        driver.append(webdriver.Remote(command_executor=ip,
                                       desired_capabilities={"browserName": "internet explorer"}))

        driver.append(webdriver.Remote(command_executor=ip,
                                       desired_capabilities={'browserName': 'firefox', 'platform': 'WINDOWS',
                                                             'platformName': 'WINDOWS'}))

        print('setup')
    except:
        print('error', '**********setUp failed**********')


def destroy():
    try:
        # closing chromedriver, this runs after every test
        for i in range(7):
            driver[i].quit()
    except:
        print('**************Failed to do tearDown***************')


class DY(unittest.TestCase):

    def clickButton(self, browser, id):
        browser.find_element_by_id(id).click()
        time.sleep(5)


    def clickNextArrow(self, browser):
        browser.find_element_by_xpath(arrowXpath).click()
        time.sleep(2)


    def getDataFromDY(self, browser, xpath):
        ele = browser.find_elements_by_xpath(xpath)
        links = browser.find_elements_by_xpath(xpath + '/a')
        names = browser.find_elements_by_xpath(xpath + '/a/div[2]/p[1]')
        return ele, links, names


    def runTests(self, testNR):
        driver[testNR].implicitly_wait(20)
        driver[testNR].get(testLink)
        time.sleep(20)
        self.clickButton(driver[testNR])
        time.sleep(2)
        #driver[testNR].find_element_by_id('btn_add_to_cart').click()             # click buy button
        xpath = '//section[contains(@class, "dy-rcom-item")]'
        ele, links, names = self.getDataFromDY(driver[testNR], xpath)   # get data
        maxEle = len(ele)
        flag = [False] * maxEle
        articles = 0
        while articles < maxEle:
            for i in range(maxEle):
                if ele[i].get_attribute("aria-hidden") == 'false' and not flag[i]:
                    link = links[i].get_attribute('href')
                    name = names[i].get_attribute('innerText')
                    ele[i].click()
                    time.sleep(5)
                    try:
                        self.assertTrue(driver[testNR].current_url == link)
                        self.assertTrue(name in driver[testNR].page_source)
                    except:
                        print('\n' + str(i) + '\n' + driver[testNR].current_url + '\n' + link + '\n' + 'testNR: ' + str(
                            testNR) + '\n' + 'name: ' + name)
                    driver[testNR].get(testLink)
                    flag[i] = True
                    articles += 1
                    time.sleep(4)
                    self.clickButton(driver[testNR], 'btn_add_to_cart')  # click buy button
                    time.sleep(2)
                    ele, links, names = self.getDataFromDY(driver[testNR], xpath)
            self.clickNextArrow(driver[testNR])


    def test_chromeMAC(self):
        self.runTests(0)

    def test_firefoxMAC(self):
        self.runTests(1)

    def test_safariMAC(self):
        self.runTests(2)

    def test_edgeWIN(self):
        self.runTests(3)

    def test_chromeWIN(self):
        self.runTests(4)

    def test_IEWIN(self):
        self.runTests(5)

    def test_firefoxWIN(self):
        self.runTests(6)


if __name__ == "__main__":
    drivers()
    suite = unittest.TestLoader().loadTestsFromTestCase(DY)
    runner = unittest.TextTestRunner()
    # runner.run(suite)     # uncomment this to run one test at a time and comment the two lines below
    concurrent_suite = ConcurrentTestSuite(suite, fork_for_tests(7))
    runner.run(concurrent_suite)
    destroy()
